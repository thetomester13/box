#!/bin/bash

set -eu -o pipefail

readonly curl="curl --fail --connect-timeout 20 --retry 10 --retry-delay 2 --max-time 2400 --http1.1"

ip=""
dns_config=""
tls_cert_file=""
tls_key_file=""
license_file=""
backup_config=""

args=$(getopt -o "" -l "ip:,backup-config:,license:,dns-config:,tls-cert:,tls-key:" -n "$0" -- "$@")
eval set -- "${args}"

while true; do
    case "$1" in
    --ip) ip="$2"; shift 2;;
    --dns-config) dns_config="$2"; shift 2;;
    --tls-cert) tls_cert_file="$2"; shift 2;;
    --tls-key) tls_key_file="$2"; shift 2;;
    --license) license_file="$2"; shift 2;;
    --backup-config) backup_config="$2"; shift 2;;
    --) break;;
    *) echo "Unknown option $1"; exit 1;;
    esac
done

# validate arguments in the absence of data
if [[ -z "${ip}" ]]; then
    echo "--ip is required"
    exit 1
fi

if [[ -z "${dns_config}" ]]; then
    echo "--dns-config is required"
    exit 1
fi

if [[ ! -f "${license_file}" ]]; then
    echo "--license must be a valid license file"
    exit 1
fi

function get_status() {
    key="$1"
    if status=$($curl -q -f -k "https://${ip}/api/v1/cloudron/status" 2>/dev/null); then
        currentValue=$(echo "${status}" | python3 -c 'import sys, json; print(json.dumps(json.load(sys.stdin)[sys.argv[1]]))' "${key}")
        echo "${currentValue}"
        return 0
    fi

    return 1
}

function wait_for_status() {
    key="$1"
    expectedValue="$2"

    echo "wait_for_status: $key to be $expectedValue"
    while true; do
        if currentValue=$(get_status "${key}"); then
            echo "wait_for_status: $key is current: $currentValue expecting: $expectedValue"
            if [[ "${currentValue}" == $expectedValue ]]; then
                break
            fi
        fi
        sleep 3
    done
}

echo "=> Waiting for cloudron to be ready"
wait_for_status "version" '*'

domain=$(echo "${dns_config}" | python3 -c 'import json,sys;obj=json.load(sys.stdin);print(obj["domain"])')

echo "Provisioning Cloudron ${domain}"
if [[ -n "${tls_cert_file}" && -n "${tls_key_file}" ]]; then
    tls_cert=$(cat "${tls_cert_file}" | awk '{printf "%s\\n", $0}')
    tls_key=$(cat "${tls_key_file}" | awk '{printf "%s\\n", $0}')
    fallback_cert=$(printf '{ "cert": "%s", "key": "%s", "provider": "fallback", "restricted": true }' "${tls_cert}" "${tls_key}")
else
    fallback_cert=None
fi

tls_config='{ "provider": "fallback" }'
dns_config=$(echo "${dns_config}" | python3 -c "import json,sys;obj=json.load(sys.stdin);obj.update(tlsConfig=${tls_config});obj.update(fallbackCertficate=${fallback_cert});print(json.dumps(obj))")

license=$(cat "${license_file}")

if [[ -z "${backup_config:-}" ]]; then
    backup_config='{ "provider": "filesystem", "backupFolder": "/var/backups", "format": "tgz" }'
fi

setupData=$(printf '{ "dnsConfig": %s, "autoconf": { "appstoreConfig": %s, "backupConfig": %s } }' "${dns_config}" "${license}" "${backup_config}")

if ! setupResult=$($curl -kq -X POST -H "Content-Type: application/json" -d "${setupData}" https://${ip}/api/v1/cloudron/setup); then
    echo "Failed to setup with ${setupData} ${setupResult}"
    exit 1
fi

wait_for_status "webadminStatus" '*"tls": true*'

echo "Cloudron is ready at https://my-${domain}"

