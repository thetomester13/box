#!/bin/bash

# This script collects diagnostic information to help debug server related issues
# It also enables SSH access for the cloudron support team

PASTEBIN="https://paste.cloudron.io"
OUT="/tmp/cloudron-support.log"
LINE="\n========================================================\n"
CLOUDRON_SUPPORT_PUBLIC_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQVilclYAIu+ioDp/sgzzFz6YU0hPcRYY7ze/LiF/lC7uQqK062O54BFXTvQ3ehtFZCx3bNckjlT2e6gB8Qq07OM66De4/S/g+HJW4TReY2ppSPMVNag0TNGxDzVH8pPHOysAm33LqT2b6L/wEXwC6zWFXhOhHjcMqXvi8Ejaj20H1HVVcf/j8qs5Thkp9nAaFTgQTPu8pgwD8wDeYX1hc9d0PYGesTADvo6HF4hLEoEnefLw7PaStEbzk2fD3j7/g5r5HcgQQXBe74xYZ/1gWOX2pFNuRYOBSEIrNfJEjFJsqk3NR1+ZoMGK7j+AZBR4k0xbrmncQLcQzl6MMDzkp support@cloudron.io"
HELP_MESSAGE="
This script collects diagnostic information to help debug server related issues

 Options:
   --enable-ssh    Enable SSH access for the Cloudron support team
   --help          Show this message
"

# We require root
if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

enableSSH="false"

args=$(getopt -o "" -l "help,enable-ssh" -n "$0" -- "$@")
eval set -- "${args}"

while true; do
    case "$1" in
    --help) echo -e "${HELP_MESSAGE}"; exit 0;;
    --enable-ssh) enableSSH="true"; shift;;
    --) break;;
    *) echo "Unknown option $1"; exit 1;;
    esac
done

# check if at least 10mb root partition space is available
if [[ "`df --output="avail" / | sed -n 2p`" -lt "10240" ]]; then
    echo "No more space left on /"
    echo "This is likely the root case of the issue. Free up some space and also check other partitions below:"
    echo ""
    df -h
    echo ""
    echo "To recover from a full disk, follow the guide at https://cloudron.io/documentation/server/#recovery-after-disk-full"
    exit 1
fi

# check for at least 5mb free /tmp space for the log file
if [[ "`df --output="avail" /tmp | sed -n 2p`" -lt "5120" ]]; then
    echo "Not enough space left on /tmp"
    echo "Free up some space first by deleting files from /tmp"
    exit 1
fi

echo -n "Generating Cloudron Support stats..."

# clear file
rm -rf $OUT

ssh_port=$(cat /etc/ssh/sshd_config | grep "Port " | sed -e "s/.*Port //")
if [[ $SUDO_USER == "" ]]; then
    ssh_user="root"
    ssh_folder="/root/.ssh/"
    authorized_key_file="${ssh_folder}/authorized_keys"
else
    ssh_user="$SUDO_USER"
    ssh_folder="/home/$SUDO_USER/.ssh/"
    authorized_key_file="${ssh_folder}/authorized_keys"
fi

echo -e $LINE"SSH"$LINE >> $OUT
echo "Username: ${ssh_user}" >> $OUT
echo "Port:     ${ssh_port}" >> $OUT

echo -e $LINE"cloudron.conf"$LINE >> $OUT
cat /etc/cloudron/cloudron.conf &>> $OUT

echo -e $LINE"Docker container"$LINE >> $OUT
if ! timeout --kill-after 10s 15s docker ps -a &>> $OUT 2>&1; then
    echo -e "Docker is not responding" >> $OUT
fi

echo -e $LINE"Filesystem stats"$LINE >> $OUT
df -h &>> $OUT

echo -e $LINE"System daemon status"$LINE >> $OUT
systemctl status --lines=100 cloudron.target box mysql unbound cloudron-syslog nginx collectd docker &>> $OUT

echo -e $LINE"Box logs"$LINE >> $OUT
tail -n 100 /home/yellowtent/platformdata/logs/box.log &>> $OUT

echo -e $LINE"Firewall chains"$LINE >> $OUT
iptables -L &>> $OUT

echo "Done"

echo -n "Uploading information..."
# for some reason not using $(cat $OUT) will not contain newlines!?
paste_key=$(curl -X POST ${PASTEBIN}/documents --silent -d "$(cat $OUT)" | python3 -c "import sys, json; print(json.load(sys.stdin)['key'])")
echo "Done"

if [[ "${enableSSH}" == "true" ]]; then
    echo -n "Enabling ssh access for the Cloudron support team..."
    mkdir -p "${ssh_folder}"
    echo "${CLOUDRON_SUPPORT_PUBLIC_KEY}" >> ${authorized_key_file}
    chown -R ${ssh_user} "${ssh_folder}"
    chmod 600 "${authorized_key_file}"
    echo "Done"
fi

echo ""
echo "Please email the following link to support@cloudron.io"
echo ""
echo "${PASTEBIN}/${paste_key}"
