'use strict';

exports = module.exports = {
    accesscontrol: require('./accesscontrol.js'),
    apps: require('./apps.js'),
    backups: require('./backups.js'),
    clients: require('./clients.js'),
    cloudron: require('./cloudron.js'),
    developer: require('./developer.js'),
    domains: require('./domains.js'),
    eventlog: require('./eventlog.js'),
    graphs: require('./graphs.js'),
    groups: require('./groups.js'),
    oauth2: require('./oauth2.js'),
    mail: require('./mail.js'),
    profile: require('./profile.js'),
    provision: require('./provision.js'),
    services: require('./services.js'),
    settings: require('./settings.js'),
    sysadmin: require('./sysadmin.js'),
    ssh: require('./ssh.js'),
    tasks: require('./tasks.js'),
    users: require('./users.js')
};
